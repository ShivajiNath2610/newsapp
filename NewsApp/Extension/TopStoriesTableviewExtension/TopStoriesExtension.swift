//
//  TopStoriesExtension.swift
//  NewsApp
//
//  Created by Onqanet on 17/03/22.
//

import Foundation
import UIKit

extension TopStoriesViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTopStories!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let topStoriesCell = tableView.dequeueReusableCell(withIdentifier: AppConstants.topStoriesCellIdentifier, for: indexPath)as! TopStoriesTableViewCell
        
        topStoriesCell.lblTitle.text = arrTopStories![indexPath.row].title
        topStoriesCell.lblDescription.text = arrTopStories![indexPath.row].articleDescription
        
        return topStoriesCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let detailedNewsVC = self.storyboard?.instantiateViewController(identifier: "DetailedNewsViewController")as! DetailedNewsViewController
        
        let detailsURL = arrTopStories![indexPath.row].url
        
        detailedNewsVC.strURLToLoad = detailsURL
        
        self.navigationController?.pushViewController(detailedNewsVC, animated: true)
    }
}
