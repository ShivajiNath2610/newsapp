//
//  Service.swift
//  NewsApp
//
//  Created by Onqanet on 17/03/22.
//

import Foundation
import Alamofire

class Service {
    
    func printServiceURL(strURL : String, strURLForService : String, methodType: String, dictParams : [String : Any]) {
        print("\n\n******************** Service Call for \(strURLForService) ********************")
        
        print("\n\n Service URL:==> \(strURL)")
        print("\nMethod Type:==> \(methodType)")
        print("\nParams:==> \(dictParams)")
                
        print("\n\n******************** End Service Call for \(strURLForService) ********************\n\n")
    }
    
    func printResponseJSON(responseData : Data, strResponseForService : String)  {
        print("\n\n##################### Response For Service \(strResponseForService) #####################")
        
        let jsonString = String(data: responseData, encoding: .utf8)!
        print("\n\(jsonString)")//Response in JSON String from Service \(strResponseForService)
        
        print("\n\n##################### End Service Call\(strResponseForService) #####################\n\n")
    }
    
    func getTopStories(forCountry country : String, completion : @escaping (_ topStoriesData : TopStoriesModel? ,_ status : Bool, _ message : String) -> Void){
        
        let modifiedURL = APIConstants.APIServicePath.getTopStories + country + "&category=business&apiKey=\(AppConstants.NEWS_API_KEY)"
        
        printServiceURL(strURL: modifiedURL, strURLForService: "Get Top Stories", methodType: "Get", dictParams: [:])
        
        
        AF.request(modifiedURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).response { (responseData) in
            guard let data = responseData.data else{
                completion(nil, false, responseData.error!.localizedDescription)
                return
            }
            
            do {
                
            let topStoriesData = try JSONDecoder().decode(TopStoriesModel.self, from: data)
                self.printResponseJSON(responseData: data, strResponseForService: "==> Get Top Stories <==")
                completion(topStoriesData, true, "Response Received Successfully")
            }catch {
                print("Error afetr decoding :==>\(error)")
                completion(nil, false, error.localizedDescription)
            }
        }
        
    }
    
    
}
