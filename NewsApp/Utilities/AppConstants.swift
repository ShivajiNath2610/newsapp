//
//  AppConstants.swift
//  NewsApp
//
//  Created by Onqanet on 17/03/22.
//

import Foundation

struct AppConstants {
    
    static let NEWS_API_KEY = "e5e3a444364b42f0829f035765ce82e9"
    
    static let topStoriesCellIdentifier = "TopStoriesCell"


}


struct APIConstants {
    static let BaseURL = "https://newsapi.org/v2/"
    
    
    
    struct APIServicePath {
        
        static let getTopStories        = BaseURL + "top-headlines?country="
    }
    
}

struct RequestParams {
    
    struct topStoryParams {
        static var country = "country"
    }
    
}


