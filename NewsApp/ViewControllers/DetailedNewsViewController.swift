//
//  DetailedNewsViewController.swift
//  NewsApp
//
//  Created by Onqanet on 17/03/22.
//

import UIKit
import WebKit

class DetailedNewsViewController: BaseViewController {

    // MARK: -
    @IBOutlet weak var webView: WKWebView!
    
    // MARK: - Variable Declaration
    var strURLToLoad : String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.barTintColor = UIColor(red: 249.0 / 255.0, green: 249.0 / 255.0, blue: 253.0 / 255.0, alpha: 1.0)
        guard let strURL = strURLToLoad else { return }
        
        let urlToLoad = URL(string: strURL)
        
        webView.load(URLRequest(url: urlToLoad!))
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
