//
//  BaseViewController.swift
//  NewsApp
//
//  Created by Onqanet on 17/03/22.
//

import UIKit
import ProgressHUD
import Alamofire

class BaseViewController: UIViewController {

    // MARK: - Variable Declarations
    let coverImageVw = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    // MARK: - Custom Methods
    
    func showLoader()  {
    
        coverImageVw.frame = UIScreen.main.bounds
        coverImageVw.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        self.view.addSubview(coverImageVw)
        
        ProgressHUD.animationType = .circleSpinFade
        ProgressHUD.colorBackground = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.25)
        ProgressHUD.colorAnimation = .init(cgColor: CGColor(srgbRed: 0.0/255.0, green: 64.0/255.0, blue: 109.0/255.0, alpha: 1.0))
        ProgressHUD.show("Loading..")

    }
    
    func dismissBackground() {
        coverImageVw.removeFromSuperview()
    }
}
