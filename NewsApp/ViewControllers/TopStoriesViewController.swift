//
//  TopStoriesViewController.swift
//  NewsApp
//
//  Created by Onqanet on 17/03/22.
//

import UIKit
import ProgressHUD

class TopStoriesViewController: BaseViewController {

    //MARK:- Outlet Declaration
    
    @IBOutlet weak var tblTopStories: UITableView!
    //MARK:- Variable Declaration
    
    let service = Service()
    
    
    var arrTopStories : [Article]?
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "News App"
        self.view.backgroundColor = .secondarySystemBackground
        
        setUp()
        
        //Call API to get the Top Stories Data
        getTopStories()
    
    }

    func setUp(){
        tblTopStories.register(UINib(nibName: "TopStoriesTableViewCell", bundle: nil), forCellReuseIdentifier: AppConstants.topStoriesCellIdentifier)
        tblTopStories.estimatedRowHeight = 90.0
        tblTopStories.rowHeight = UITableView.automaticDimension
    }
    
    
    func getTopStories(){
        print("Country Code :==>\(Locale.current.regionCode!)")
        self.showLoader()
        
        service.getTopStories(forCountry: Locale.current.regionCode!) { (result, status, msg) in
            if status{
                ProgressHUD.dismiss()
                self.dismissBackground()
                guard let _response = result else{return}
                
                print("Respose for Top Stories = \(_response.articles?.count)")
                self.arrTopStories = _response.articles
                self.tblTopStories.delegate = self
                self.tblTopStories.dataSource = self
                
                self.tblTopStories.reloadData()
            }
        }
        
        
        
    }
}
