//
//  TopStoriesTableViewCell.swift
//  NewsApp
//
//  Created by Onqanet on 17/03/22.
//

import UIKit

class TopStoriesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var vwDataHolder: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        vwDataHolder.layer.cornerRadius = 15
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
